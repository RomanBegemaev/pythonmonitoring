import subprocess
import yaml
import os
from typing import Dict, Any

inventory: Dict[str, Any] = {}

# Запрос ввода IP адресов серверов
ipAddresses = input("Enter server IP addresses: ")
ipList = [ip.strip() for ip in ipAddresses.split()]

# Запрос ввода Delay
delay = input("Enter Delay in sec: ")

# Запрос ввода async
aSync = input("Enter async in sec: ")

# Запрос ввода retries
retries = input("Enter retries: ")

scriptDir = os.path.dirname(os.path.abspath(__file__))

# Начало генерации инвентаря с занесением IP адресов в список хостов
file_path = os.path.join(scriptDir, 'dynamicInventory.yaml')
inventory = {
    'all': {
        'hosts': {}
    }
}

for ip in ipList:
    inventory['all']['hosts'][ip] = {
        'ansible_host': ip
    }

with open(file_path, 'w') as file:
    yaml.dump(inventory, file, default_flow_style=False)

print("Dynamic inventory has been created successfully.")
# Конец генерации

# Генерация конфига времени
file_path = os.path.join(scriptDir, 'configDelay.yaml')
config = {
    'myAsync': int(aSync),
    'myDelay': int(delay),
    'myRetries': int(retries)
}

with open(file_path, 'w') as file:
    yaml.dump(config, file)

print("configDelay has been created successfully.")
# Конец генерации

# Команда для запуска плейбука с указанием инвентаря
command = f'ansible-playbook -i dynamicInventory.yaml ./pythonmonitoring/ansMonitor.yml'

# Запуск команды субпроцессом
process = subprocess.Popen(command, shell=True)
process.wait()